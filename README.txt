This is a German profile for Drupal installation.

In order to utilize it fully, one should follow the steps below:

== Install Drupal ==
* Download and extract drupal-5.2
* Create the directories 'sites/all/modules' and 'sites/all/themes'

== Copy Installation Profile ==

* Copy this directory onto the 'profiles/' directory
* Rename the file 'de.po.txt' to 'de.po'

== The autolocale module ==

* Download from 'http://drupal.org/project/autolocale' (NOTE: currently, there is no available download link on that page. You will have to download the module from the CVS until this is fixed)
* Copy the autolocale directory onto 'sites/all/modules'

== German translation ==
* Download and extract latest German translation from 'http://drupal.org/project/de'
* Copy the file de.po from the translation onto 'sites/all/modules/autolocale/po/drupal-<version>.de.po'
* Copy the file installer.po onto 'sites/all/modules/autolocale/po/installer-<version>.de.po'



